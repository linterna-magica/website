// @licstart  The following is the entire license notice for the
//  JavaScript code in this page.
//
//  Copyright (C) 2010  Anton Katsarov <anton@katsarov.org>
//  Copyright (C) 2010, 2011  Ivaylo Valkov <ivaylo@e-valkov.org>
//
//  The JavaScript code in this page is free software: you can
//  redistribute it and/or modify it under the terms of the GNU
//  General Public License (GNU GPL) as published by the Free Software
//  Foundation, either version 3 of the License, or (at your option)
//  any later version.  The code is distributed WITHOUT ANY WARRANTY
//  without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
//
//  As additional permission under GNU GPL version 3 section 7, you
//  may distribute non-source (e.g., minimized or compacted) forms of
//  that code without the copy of the GNU GPL normally required by
//  section 4, provided you include this license notice and a URL
//  through which recipients can access the Corresponding Source.
//
//  @licend  The above is the entire license notice
//  for the JavaScript code in this page.
//
// @source http://e-valkov.org/linterna-magica/js/plugins-page.js

window.addEvent('domready', function () {
	// Remove the single image static header gallery
	// Clone the screenshots and insert them as the new gallery
	var gallery = $("gallery-static").clone();
	gallery.setProperty("id","gallery-clone");
        gallery.removeClass("noshow");
	gallery.inject("gallery", "before");
	$("gallery").destroy();
	$("gallery-clone").setProperty("id", "gallery");
        $("gallery").removeClass("noshow");

	new viewer($$('#gallery a'),{
		mode: 'alpha',
		    interval: 5000
		    }).play(true);

    // Slimbox had processed the page before the dynamic gallery was created
    // If the scanPage methos is used again we wil have duplicate of each image.
    // We stop the default processing of the click, and fire a click event on the 
    // <a> element form the static-gallery with the same index (same image).
    // These elements are processed by Slimbox, so we have a viewer ;)
    $$('#gallery a').each(function(el, index) {
	el.addEvent('click', function(e){
	    new Event(e).stop();
	    $$('#gallery-static a')[index].fireEvent('click');
	});
    });
});